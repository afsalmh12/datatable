import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  dataSet: any;
  columns: any;
  searchText = "";
  selectedCell = "none";
  selectedColumn = "none";
  asc = false;
  valueChanged = true;
  resposeState = false;
  deletedRows = [];
  updatedRows = [];
  constructor(private dataService: DataService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.bindData();
  }

  //binding data from service
  bindData() {
    this.dataService.getData().subscribe((data) => {
      this.dataSet = data;
      let totalcoulmns = [];
      for (var row in data) {
        let keys = Object.keys(data[row]);
        let _idIndex = keys.indexOf('_id');
        if (_idIndex > -1) {
          keys.splice(_idIndex, 1)
        }
        //spred operator to concatenate two arrays
        totalcoulmns = [...totalcoulmns, ...keys];
      }
      this.columns = [...new Set(totalcoulmns)]
      this.resposeState = true;
    })
  }

  //function to enable selected cell


  isSelectedCell(cell: string) {
    return (cell == this.selectedCell)
  }


  //function to select a cell for editing
  onDoubleClick(cell: string) {
    this.selectedCell = cell;
  }

  updatedRowValue(row) {
    if (this.updatedRows.indexOf(row) == -1) {
      this.updatedRows.push(row);
    }
  }

  //function to update value change in table
  onChange(event: any, row: any, column: any) {
    this.dataSet[row][column] = event.target.value;
    this.valueChanged = false;
    this.updatedRowValue(row);
  }

  addRow() {
    let row = {};
    this.columns.forEach(function (key) {
      row[key] = "";
    })
    this.dataSet.push(row);
    this.updatedRowValue(row);
  }

  deleteRow(index) {
    if (this.dataSet[index]._id) {
      this.deletedRows.push(this.dataSet[index]._id);
    }
    this.dataSet.splice(index, 1);
    this.valueChanged = false;
    this.generateSnackBar("Data deleted");
  }
  save() {
    this.valueChanged = true;
    this.resposeState = false;
    let updatedData = [];

    for (let row in this.updatedRows) {
      if (this.dataSet[this.updatedRows[row]]) {
        updatedData.push(this.dataSet[this.updatedRows[row]]);
       
      }
    }
    //console.log(updatedData)
    this.dataService.saveData(updatedData, this.deletedRows).subscribe((response) => {
      if (response["status"] == "success") {
        this.deletedRows = [];
        this.updatedRows = [];
        this.bindData();
        this.generateSnackBar("Data saved");

      }
      else
        this.generateSnackBar("Failed to update");
      this.resposeState = true;
    },
      error => {
        console.log(error);
        this.generateSnackBar("Failed to update");
        this.resposeState = true;
      }
    )
  }

  generateSnackBar(message) {
    this._snackBar.open(message, "", {
      duration: 1000,
      panelClass: ['blue-snackbar']
    });

  }

}
