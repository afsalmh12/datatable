import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})

export class DataService {
  
  private _baseUrl =  "http://localhost:3000/";
  constructor(private http:HttpClient) {  }

  getData(){
    return this.http.get(this._baseUrl+"getEmployeeDetails");
  }
  saveData(data,deletedRows){
    return this.http.post(this._baseUrl+"updateEmployeeDetails",{
      data : data,
      deletedRows : deletedRows
    })
  }
}
