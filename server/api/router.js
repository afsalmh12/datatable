const router = require('express').Router()
const dataController = require('../controllers/data');
router.get('/getEmployeeDetails', dataController.getEmployeeDetails);
router.post('/updateEmployeeDetails',dataController.updateEmployeeDetails);
module.exports = router