const mongoose = require('mongoose');
const schema = mongoose.Schema;
//defining user schema
const employeeSchema = new schema({
    // _id:schema.Types.ObjectId,
    first_name: String,
    last_name:String,
    address:String,
    city:String,
    email:String,
    phone:String
},{
    versionKey: false 
})
//exporting user schema
module.exports = mongoose.model("employee",employeeSchema,"employee");