const express = require('express')
const bodyParser = require('body-parser')
const router = require('./api/router');
const mongo = require('./db_config/mongodb');
var cors = require('cors')
const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(router);
app.listen(3000);