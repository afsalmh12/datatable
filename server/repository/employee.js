const employee = require('../models/employee');
const mongoose = require('mongoose');

//function to get employee details
function getEmployeeDetaills() {
    return new Promise(function (resolve, reject) {
        employee.find({})
            .then((result) => {
                resolve(result)
            })
            .catch((err) => {
                reject(err);
            })
    })
}


var updateEmployeeDetails = (employeeDetails) => {
    return new Promise(function (resolve, reject) {
        if(!employeeDetails._id){
        mongoose.connection.db.collection('employee').insertOne(employeeDetails)
            .then((data) => {
                resolve("success");
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
        }else{
            let id = mongoose.Types.ObjectId(employeeDetails._id);
            delete employeeDetails._id;
            mongoose.connection.db.collection('employee').findOneAndReplace({_id:id},employeeDetails)
            .then((data) => {
                resolve("success");
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
        }
    })
}

var removeEmployee = (employeeIds) => {
    return new Promise(function (resolve, reject) {
        employee.deleteMany({ _id: { $in: employeeIds } })
            .then((data) => {
                resolve("success");
            })
            .catch((err) => {
                reject(err);
            })

    })
}

module.exports = {
    'getEmployeeDetaills': getEmployeeDetaills,
    'updateEmployeeDetails': updateEmployeeDetails,
    'removeEmployee': removeEmployee
};