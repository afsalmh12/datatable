const employee_repo = require('../repository/employee');


var getEmployeeDetails = (req, res) => {
    employee_repo.getEmployeeDetaills()
        .then((employees) => {
            res.json(employees);
        })
        .catch((err) => {
            res.json({ status: "Failed" })
        })
}

var updateEmployeeDetails = async (req,res) => {
    let updatedEmployees = req.body.data;
    let deletedEmployees = req.body.deletedRows;
    let response = "";
    if(updatedEmployees.length > 0){
        for(let element in updatedEmployees){
            response = await employee_repo.updateEmployeeDetails(updatedEmployees[element])

        }  
    }
    if(deletedEmployees.length > 0){
      response = await employee_repo.removeEmployee(deletedEmployees)
        
    }
    res.json({"status":response})
}
module.exports = {
    getEmployeeDetails: getEmployeeDetails,
    updateEmployeeDetails : updateEmployeeDetails
};