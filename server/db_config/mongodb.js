const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/datatable', { useNewUrlParser: true ,
useCreateIndex: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'DB connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("DB connection successful");
});
