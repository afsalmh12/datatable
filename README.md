#To run front end

change the directory to 'client' and install dependencies

=> npm install
=> ng serve

#To run back end

#change the directory to 'server' and install dependencies

=> npm install
=> node server.js


#import the sample data to mongodb

=> mongoimport --db datatable --collection employee --file sampledata.json
